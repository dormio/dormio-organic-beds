Dormio Organic Beds is a trusted retailer of organic mattresses, pillows and sheets. With organic bedding showrooms in Kitchener, Mississauga, Toronto and Vaughan, we can serve customers from any part of the GTA. Our high-quality natural latex foam mattresses are covered with organic cotton and padded further with organic wool ensuring a healthier sleeping experience for your family. We also have specially designed soft crib mattresses for babies. Some of our crib mattress collections are Lynn Crib, Addison Crib, Jordan Crib, Parker Crib and Logan Crib Mattresses.

Website: https://dormio.ca/
